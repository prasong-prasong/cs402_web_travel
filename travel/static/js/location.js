var updateBtns = document.getElementsByClassName('update-cart')

for (i = 0; i < updateBtns.length; i++) {
	updateBtns[i].addEventListener('click', function(event){
		event.preventDefault()
		
		var locationID = this.dataset.location
		var action = this.dataset.action
		console.log('locationID:', locationID, 'Action:', action)
		console.log('USER:', user)

		if (user == 'AnonymousUser'){
			addCookieItem(locationID, action)
		} else {
			updateUserOrder(locationID, action)
		}
	})
}

function updateUserOrder(locationID, action){
	console.log('User is authenticated, sending data...')

		var url = "/en/trip/update_item/"
		// var url = "{% url 'trip:update_item' %}"

		fetch(url, {
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				'X-CSRFToken':csrftoken,
			}, 
			
			body:JSON.stringify({'locationID':locationID, 'action':action})
		})
		.then((response) => {
		   return response.json();
		})
		.then((data) => {
		    location.reload()
		});
}

function addCookieItem(locationID, action){
	console.log('User is not authenticated')

	if (action == 'add'){
		if (cart[locationID] == undefined){
			cart[locationID] = {'time':30}
			Swal.fire({
				// position: 'top-end',
				icon: 'success',
				title: 'เพิ่มสถานที่สำเร็จแล้วในการเดินทาง',
				showConfirmButton: false,
				timer: 1500
			}).then((result) => {
				window.location.reload();
			})
		} else {
			cart[locationID]['time'] += 30
			Swal.fire({
				// position: 'top-end',
				icon: 'success',
				title: 'เพิ่มเวลาสถานที่การเดินทาง\n(เพิ่ม 30 นาที)',
				showConfirmButton: false,
				timer: 1500
			}).then((result) => {
				window.location.reload();
			})
		}
	}


	if (action == 'remove'){
		cart[locationID]['time'] -= 30
		if (cart[locationID]['time'] <= 0){
			console.log('Item should be deleted')
			delete cart[locationID];
		}
		window.location.reload();
	}

	if (action == 'clear'){
		console.log('Item should be deleted')
			// Swal.fire({
			// 	title: 'คุณแน่ใจไหม?',
			// 	text: "คุณต้องการลบสถานที่หรือไม่!",
			// 	icon: 'warning',
			// 	showCancelButton: true,
			// 	confirmButtonColor: '#3085d6',
			// 	cancelButtonColor: '#d33',
			// 	confirmButtonText: 'ฉันต้องการลบ!',
			// 	cancelButtonText: 'ยกเลิก'
			// }).then((result) => {
			// 	if (result.value) {
			// 	Swal.fire(
			// 		'ลบ!',
			// 		'สถานที่ที่คุณเลือกคุณถูกลบ.',
			// 		'สำเร็จ',
			// 	)}
			// 	setTimeout(function(){
			// 		window.location.reload();
			// 	 }, 1000);
			// })
			// .then((result) => {
			// 	window.location.reload();
			// })
			delete cart[locationID];
			window.location.reload();
	}
	
	console.log('LOCATION:', cart)
	document.cookie ='cart=' + JSON.stringify(cart) + ";domain=;path=/"
	
	
}