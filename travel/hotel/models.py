from django.db import models

# Create your models here.
from django.db import models
# from django.contrib.auth.models import User
from django.urls import reverse
from account.models import Partners, Account


# Stores all the hotel details and is used to query hotels.
class Hotels(models.Model):
    th_name                 = models.CharField(max_length=255)
    en_name                 = models.CharField(max_length=100, null=True)
    address                 = models.CharField(max_length=255, null=True)
    city                    = models.CharField(max_length=100, null=True)
    # country                 = models.CharField(max_length=100, null=True)
    zipcode                 = models.CharField(max_length=10, null=True)
    telephone_number        = models.CharField(max_length=12, null=True)
    image_path              = models.CharField(max_length=255, null=True)
    created_at              = models.DateTimeField(auto_now_add=True)
    created_at              = models.DateTimeField(auto_now=True)
    th_brief_description    = models.TextField(max_length=500, null=True)
    en_brief_description    = models.TextField(max_length=500, null=True)
    partner                 = models.ForeignKey(Partners, on_delete=models.CASCADE)
    time_check_in           = models.TimeField(blank=True, null=True)
    time_check_out          = models.TimeField(blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Hotels'

    def get_absolute_url(self):
        return reverse('hoteldetails', kwargs={'pk': self.pk})

    def __str__(self):
        return self.th_name


# Stores the Hotel reviews and is used to query the reviews
class Review(models.Model):
    user        = models.ForeignKey(Account, on_delete=models.CASCADE)
    hotel       = models.ForeignKey(Hotels, on_delete=models.CASCADE)
    comment     = models.CharField(max_length=255)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)
    rating      = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = 'Review'

    def __str__(self):
        return self.comment


# Stores the Hotels rooms
class Room(models.Model):
    hotel       = models.ForeignKey(Hotels, on_delete=models.CASCADE)
    room_type   = models.CharField(max_length=255)
    capacity    = models.IntegerField(default=0)
    bed_option  = models.CharField(max_length=255)
    price       = models.IntegerField(default=0)
    overview    = models.CharField(max_length=255)
    total_rooms = models.CharField(max_length=255)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)
    photo_1     = models.CharField(max_length=255, blank=True, null=True)
    photo_2     = models.CharField(max_length=255, blank=True, null=True)
    photo_3     = models.CharField(max_length=255, blank=True, null=True)
    photo_4     = models.CharField(max_length=255, blank=True, null=True)
    photo_5     = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Room'

    def __str__(self):
        return self.room_type


# Stores and is used to query partner proposals
class Proposal(models.Model):
    user            = models.ForeignKey(Account, on_delete=models.CASCADE)
    company_name    = models.CharField(max_length=255)
    companyEmail    = models.EmailField(max_length=254)
    HQAddress       = models.CharField(max_length=255)
    vision          = models.TextField(max_length=400)

    class Meta:
        verbose_name_plural = 'Proposal'

    def __str__(self):
        return self.company_name


# Create a Reservation Model which stores booking details
class Reservation(models.Model):
    hotel           = models.ForeignKey(Hotels, on_delete=models.CASCADE)
    room            = models.ForeignKey(Room, on_delete=models.CASCADE)
    user            = models.ForeignKey(Account, on_delete=models.CASCADE)
    guest_firstName = models.CharField(max_length=255)
    guest_lastName  = models.CharField(max_length=255)
    check_in        = models.DateField()
    check_out       = models.DateField()
    total_price     = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = 'Reservation'

    class Meta:
        verbose_name_plural = 'Reservation'

    def __str__(self):
        return '{} - {}'.format(self.guest_firstName, self.guest_lastName)


# Stating the Photo model , and giving it a file field to allow photo uploads.
class Photo(models.Model):
    path        = models.FileField()
    hotel       = models.ForeignKey(Hotels, on_delete=models.CASCADE)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Photo'

    def __str__(self):
        return '{} - {}'.format(self.hotel.en_name, self.hotel.th_name)
