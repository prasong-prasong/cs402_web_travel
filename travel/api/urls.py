from django.urls import path, include
from rest_framework.routers import DefaultRouter
from api.views import changeLanguage
from api.views import CategoryMainViewSet, CategorySubViewSet, LocationViewSet, LocationGalleryViewSet, TripViewSet, StyleViewSet

router = DefaultRouter()

router.register('api_category_main', CategoryMainViewSet)
router.register('api_category_sub', CategorySubViewSet)
router.register('api_location', LocationViewSet)
router.register('api_location_gallery', LocationGalleryViewSet)

router.register('api_trip', TripViewSet)
router.register('api_trip_style', StyleViewSet)


app_name = 'api'

urlpatterns = [
    path('', include(router.urls), name="api_location"),
    path('change/<slug:language>', changeLanguage, name="change-language"),

]