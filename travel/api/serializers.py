from rest_framework import serializers
from location.models import CategoryMain, CategorySub, Location, LocationGallery
from trip.models import Trip, Style

# ========================== LOCATION MODELS ==========================
class CategoryMainSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryMain
        fields = '__all__'


class CategorySubSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategorySub
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        # fields = ('name', 'category_sub', 'cost', 'lat', 'lng', 'time_open', 'time_close', 'shop', 'detail', 'tags', 'type_adventure', 'type_season', 'suitable_person')
        fields = '__all__'


class LocationGallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = LocationGallery
        fields = '__all__'
# ========================== END LOCATION MODELS ==========================


# ========================== TRIP MODELS ==========================

class StyleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Style
        fields = '__all__'


class TripSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trip
        fields = '__all__'

# ========================== END TRIP MODELS ==========================
