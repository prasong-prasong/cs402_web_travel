from django.urls import include, path

from general.views import (
    contact_view,
    contact_detail,
)

app_name = 'general'

urlpatterns = [

    path('', contact_view, name='contact_view'),
    path('detail/<str:name>', contact_detail, name='contact_detail'),

]

