from django.shortcuts import render, get_object_or_404
from location.models import Location
from trip.models import Trip, Style
from general.models import Contact, ContactDetail
from taggit.models import Tag

import csv
from django.http import HttpResponse

def home_view(request):
	location = Location.objects.all().count()
	style = Style.objects.all().count()
	trip = Trip.objects.all().count()
	# tags = Tag.objects.all().count()
	# tags = tags - 1
	common_tags = Trip.tags.most_common().count()
	
	data = {
		'location': location,
		'tags': common_tags,
		'style': style,
		'trip': trip,
	}
	return render(request, "general/home.html", { 'data' : data })


def contact_view(request):
	contacts = Contact.objects.all()
	print(contacts)
	count = 0
	contact_list = []
	for item in contacts:
		contact_list.append(item)

	print(contact_list)
	
	data = {
		'contacts': contacts,
		'contact_list': contact_list,
	}
	return render(request, "general/contact.html", { 'data': data })


def contact_detail(request, name):
	contact_detail = ContactDetail.objects.filter(contact=name)
	count = 0
	contact_detail_list = []
	for item in contact_detail:
		contact_detail_list.append(item.name)
		count += 1

	data = {
		'contact_detail': contact_detail,
		'contact_detail_list': contact_detail_list,
	}

	return render(request, "general/contact_detail.html", { 'data':data })


def contact_hospital(request):
	# contact_list = ["โรงพยาบาล", "สถานีตํารวจ", "สถานีดับเพลิง", "รถกู้ภัย"]

	hospital_name = ["รพ.พหลพลพยุหเสนา", "รพ.บ่อพลอย", "รพ.มะการักษ์", "รพ.สถานพระบารมี", "รพ.ด่านมะขามเต้ีย",
				"รพ.เจ้าคุณไพบูลย์พนมทวน", "รพ.ห้วยกระเจา", "รพ.ท่าม่วง", "รพ.ศุกร์สิริศรีสวัสดิ์", "รพ.ทองผาภูมิ", 
				"รพ.สังขละบุรี", "รพ.เลาขวัญ", "รพ.ไทรโยค", "รพ.ค่ายสุรสีห์", "รพ.ท่ากระดาน", "รพ.สมเด็จพระปิยมหาราชรมณียเขต",]
	hospital_phone = [" "]
	hospital_email = [" "]
	hospital_address = [" "]

	count = 0
	hospital = []
	for item in hospital_name:
		hospital_line = []
		hospital_line.append(hospital_name[count])
		hospital_line.append(hospital_phone[0])
		hospital_line.append(hospital_email[0])
		hospital_line.append(hospital_address[0])
		hospital.append(hospital_line)
		count += 1

	print(hospital)

	data = {
		'hospital': hospital,
	}

	return render(request, "general/contact_hospital.html", { 'data': data })

