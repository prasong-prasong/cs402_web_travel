import csv
from django.http import HttpResponse
from account.models import Account
from question.models import Subject, Active, Question, TakenQuiz, Quiz, Answer

def some_view(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
    
    subjects = Subject.objects.all()
    quizs = Quiz.objects.all()
    questions = Question.objects.all()

    print('===================================')
    print(subjects)
    print(quizs)
    print(questions)
    print(questions[0])
    print(questions[0].quiz)
    print(questions[0].quiz.subject)
    print('===================================')

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    
    writer.writerow(['Owner', 'Subject', 'Quiz', 'Question', 'Baz'])      # First row
    # for subject in subjects:
    # for quiz in quizs:

    for question in questions:
        writer.writerow([question.quiz.owner , question.quiz.subject, question.quiz, question])

    return response