import json

def result_table(result, result_table, result_table_txt):
    encode_result_table = result[result_table]
    txt = result[result_table_txt]
    classes_list = []
    count = 0
    classes_result  = '{ "' + result_table + '" : [ {'
    count_max = len(encode_result_table)
    for item in encode_result_table:
        line = []
        classes_result += '"' + str(int(encode_result_table[count])) + '": '
        classes_result += '"' + str(txt[count]) + '"'
        if count < count_max-1:
            classes_result += ', '
        count += 1
    classes_result += '} ] }'
    classes_result_json = json.loads((classes_result))
    classes_result_json = classes_result_json[result_table][0]
    return classes_result_json

def key_value(list, value_name):
    for key, value in list.items():
        if value == value_name:
            value_name = key
            # print ("{} {}".format(key, value))      
    return value_name
    