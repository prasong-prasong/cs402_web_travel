from django.urls import path
from blog.views import (
    home_screen_view,
	create_blog_view,
	detail_blog_view,
	edit_blog_view,
    tagged,
)

app_name = 'blog'

urlpatterns = [
    path('home/', home_screen_view, name="home" ),
    path('create/', create_blog_view, name="create"),
    path('<slug>/', detail_blog_view, name="detail"),
    path('<slug>/edit/', edit_blog_view, name="edit"),
    path('<slug>/tag/', tagged, name="tagged"),

 ]
