from django.urls import include, path
# from hotel.views import 
# from ManageHotels.views import ChartData,ChartView

from staff.views import (
    staff, location, category, question, season, other, style, landmark, contact, hotel,
    partner,
    )

# app_name = 'staff'

urlpatterns = [

    path('', staff.home_view, name='home_admin'),

    path('location/', include(([
        path('', location.location_view, name='location_view'),
        path('image/', location.LocationGalleryCreateView.as_view(), name='location_image'),
        path('create/', location.location_create_view, name='location_create'),
        path('detail/<int:pk>/', location.LocationDetail, name='location_detail'),
        # path('update/<int:pk>/', location.location_update_view, name='location_update'),
        path('update/<int:pk>/', location.LocationUpdateView.as_view(), name='location_update'),
        path('delete/<int:pk>/', location.LocationDeleteView.as_view(), name='location_delete'),
    ], 'staff'), namespace='location_admin')),    


    path('category/', include(([
        path('', category.CategoryListView.as_view(), name='category_view'),
        path('create/', category.CategoryCreateView.as_view(), name='category_create'),
        path('update/<int:pk>/', category.CategoryUpdateView.as_view(), name='category_update'),
        path('delete/<int:pk>', category.CategoryDeleteView.as_view(), name='category_delete'),
    ], 'staff'), namespace='category_admin')),    


    path('question/', include(([
        path('', question.QuizListView.as_view(), name='quiz_change_list'),
        path('quiz/add/', question.QuizCreateView.as_view(), name='quiz_add'),
        path('quiz/results/view', question.results_view, name='results_view'),
        path('quiz/<int:pk>/', question.QuizUpdateView.as_view(), name='quiz_change'),
        path('quiz/<int:pk>/delete/', question.QuizDeleteView.as_view(), name='quiz_delete'),
        path('quiz/<int:pk>/results/', question.QuizResultsView.as_view(), name='quiz_results'),
        path('quiz/<int:pk>/question/add/', question.question_add, name='question_add'),
        path('quiz/<int:quiz_pk>/question/<int:question_pk>/', question.question_change, name='question_change'),
        path('quiz/<int:quiz_pk>/question/<int:question_pk>/delete/', question.QuestionDeleteView.as_view(), name='question_delete'),
        path('generate/', question.generate_report, name='generate_report'),
    ], 'staff'), namespace='question_admin')),

    # path('other/', other.other_staff, name='other_staff'),
    path('contact/', other.contact_staff, name='contact_staff'),


    # path('season/', include(([
    #     path('create/', season.SeasonCreateView.as_view(), name='season_create'),
    #     path('update/<int:pk>/', season.SeasonUpdateView.as_view(), name='season_update'),
    #     path('delete/<int:pk>/', season.SeasonDeleteView.as_view(), name='season_delete'),
    # ], 'staff'), namespace='season_admin')),

    # path('style/', include(([
    #     path('create/', style.StyleCreateView.as_view(), name='style_create'),
    #     path('update/<int:pk>/', style.StyleUpdateView.as_view(), name='style_update'),
    #     path('delete/<int:pk>/', style.StyleDeleteView.as_view(), name='style_delete'),
    # ], 'staff'), namespace='style_admin')),


    # path('landmark/', include(([
    #     path('create/', landmark.LandmarkCreateView.as_view(), name='landmark_create'),
    #     path('update/<int:pk>/', landmark.LandmarkUpdateView.as_view(), name='landmark_update'),
    #     path('delete/<int:pk>/', landmark.LandmarkDeleteView.as_view(), name='landmark_delete'),
    # ], 'staff'), namespace='landmark_admin')),


    path('contact/', include(([
        path('create/', contact.ContactCreateView.as_view(), name='contact_create'),
        path('update/<int:pk>/', contact.ContactUpdateView.as_view(), name='contact_update'),
        path('delete/<int:pk>/', contact.ContactDeleteView.as_view(), name='contact_delete'),
    ], 'staff'), namespace='contact_admin')),


     path('hotel/', include(([
     # path('', views.showhotels, name='showhotel'),
        path('', hotel.home, name='home'),

        path('hotel', hotel.showhotels, name='hotel_view'),
        path('hotel/create', hotel.HotelCreateView.as_view(), name='hotel_create'),
        path('hotel/update/(<pk>[0-9]+)', hotel.HotelUpdateView.as_view(), name='hotel_update'),
        path('hotel/delete/(<pk>[0-9]+)', hotel.HotelDeleteView.as_view(), name='hotel_delete'),

          # Bookings
        path('hotel/bookings/(<pk>[0-9]+)', hotel.showreservations, name='hotel_reservations'),
        path('hotel/bookings/delete/(<pk>[0-9]+)', hotel.cancelbooking, name='booking_remove'),
          
          
          # Room
        path('hotel/room/(<pk>[0-9]+)', hotel.managehotel, name='room_view'),
        path('hotel/rooms/create/(<pk>[0-9]+)', hotel.RoomCreateView.as_view(), name='room_create'),
        path('hotel/rooms/update/(<pk>[0-9]+)', hotel.RoomUpdateView.as_view(), name='room_update'),
        path('hotel/rooms/delete/(<pk>[0-9]+)', hotel.RoomDeleteView.as_view(), name='room_delete'),
          
          # Photo
        path('hotel/photo/(<id>[0-9]+)', hotel.showphotodash, name='photo_view'),
        path('hotel/photo/upload(<id>[0-9]+)', hotel.BasicUploadView.as_view(), name='photo_upload'),
        path('hotel/photo/delete/(<id>[0-9]+)', hotel.deletePhoto, name='photo_delete'),
        path('hotel/photo/thumbnail(<id>[0-9]+)', hotel.editThumbnail, name='editThumb'),

          # Chart
        path('charts/(<id>[0-9]+)/', hotel.ChartView.as_view(), name = 'partner_charts'),
        path('chart/data/(<id>[0-9]+)/', hotel.ChartData.as_view()),


        path('zxc', partner.displayDash, name='displayDash'),

        path('partners', partner.showPartners, name='showpartners'),
        path('partners/remove/(<id>[0-9]+)', partner.removePartner, name='removepartner'),

        path('proposals', partner.showProposals, name='showproposals'),
        path('proposals/accept/(<id>[0-9]+)', partner.acceptProposals, name='acceptproposal'),
        path('proposals/decline/(<id>[0-9]+)', partner.declineProposals, name='declineproposal'),
        
        path('partner/checkstatus', partner.checkstatus, name='proposal_check'),
    
     ], 'staff'), namespace='hotel_admin')),

]
