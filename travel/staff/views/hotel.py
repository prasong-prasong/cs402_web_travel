import datetime, json
from django.db.models import Sum
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
# from django.core.urlresolvers import reverse,reverse_lazy
from django.urls import reverse, reverse_lazy
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from django import forms
from hotel.models import Hotels, Review, Room, Reservation, Photo
from staff.forms import PhotoForm
from django.views import generic, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.template import RequestContext
from account.models import Partners


# Show the Partner Homepage if the request is made by a partner.
def home(request):
    user = request.user
    partner = user.partners
    PartnerHotel = Hotels.objects.filter(partner_id=partner.id).count()
    print(partner.id)
    if partner:
        context = {'Partner': partner, 'NumHotel': PartnerHotel}
        return render(request, 'staff/hotel/home.html', context)
    else:
        return HttpResponse("Error")


# Show the partner their hotels.
@login_required
def showhotels(request):

    # json_file = open('static/json/hotel.json', 'r')
    # provinces = json.load(json_file)
    # province_list = []
    # for item in provinces:
    #     en_name = item["hotel_name"]
    #     th_name = item["hotel_translated_name"]
    #     address = item["addressline1"]
    #     zipcode = item["zipcode"]
    #     # time_check_out = item["checkout"]
    #     total_rooms = item["numberrooms"]
    #     overview = item["overview"]
    #     photo1 = item["photo1"]
    #     photo2 = item["photo2"]
    #     photo3 = item["photo3"]
    #     photo4 = item["photo4"]
    #     photo5 = item["photo5"]

    #     if(item["checkin"] == ""):
    #         time_check_in = "14:00:00"
    #     else:
    #         time_check_in = item["checkin"]
            
    #     if(item["checkout"] == ""):
    #         time_check_out = "12:00:00"
    #     else:
    #         time_check_out = item["checkout"]

    #     # time_check_in = time_check_in.strptime(date_string, "YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]")
    #     # time_check_in = time_check_out.strptime(date_string, "YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]")

    #     partner = Partners.objects.filter(CompanyName = "Prasong-prasong")
    #     partner = partner[0]
    #     hotel = Hotels(th_name=th_name,en_name=en_name, address=address, city="กาญจนบุรี", th_brief_description=overview, time_check_in=time_check_in, 
    #     time_check_out=time_check_out, zipcode=zipcode, partner=partner)
    #     hotel.save()
    #     room = Room(hotel = hotel, total_rooms=total_rooms, overview=overview, room_type="normal", 
    #       capacity=2, price=500, bed_option="normal", photo_1=photo1, photo_2=photo2, photo_3=photo3, photo_4=photo4, photo_5=photo5)
    #     room.save()

    user = request.user
    thepartner = Partners.objects.get(userID=user)
    hotels_list = Hotels.objects.filter(partner=thepartner)
    context = {'Hotels': hotels_list}
    return render(request, 'staff/hotel/hotel_view.html', context)

# Show the partner reservations for specific hotels.


def showreservations(request, pk):
    thehotel = Hotels.objects.get(id=pk)
    Bookings = Reservation.objects.filter(hotel=thehotel)
    context = {'reservations': Bookings, 'hotel': thehotel}
    return render(request, 'staff/hotel/hotel_reservation.html', context)


def managehotel(request, pk):
    thehotel = Hotels.objects.get(id=pk)
    rooms = Room.objects.filter(hotel=thehotel)
    context = {
        'Hotel': thehotel,
        'rooms': rooms,
    }
    return render(request, 'staff/hotel/hotel_room.html', context)


def editThumbnail(request, id):
    if request.method == 'POST' and request.FILES['thumb']:
        thumb = request.FILES['thumb']
        fs = FileSystemStorage()
        filename = fs.save(thumb.name, thumb)
        name = thumb.name
        thehotel = Hotels.objects.get(id=id)
        photos = Photo.objects.filter(hotel=thehotel)
        thumbnail = photos.first()
        thumbnail.path = name
        thumbnail.save()
        link = reverse('hotel_admin:photo_view', args=[id])
        return HttpResponseRedirect(link)


def deletePhoto(request, id):
    photo = Photo.objects.get(id=id)
    hotel = photo.hotel
    photo.delete()
    link = reverse('hotel_admin:photodash', args=[hotel.id])
    return HttpResponseRedirect(link)


def showphotodash(request, id):
    thehotel = Hotels.objects.get(id=id)
    photos = Photo.objects.filter(hotel=thehotel)
    thumbnail = photos.first()
    context = {'Hotel': thehotel, 'Photos': photos, 'Thumbnail': thumbnail}
    return render(request, 'staff/hotel/photo_dash.html', context)


# use a django defined view to show the image upload.
# This view also handles post retrieving and returning a json response.
class BasicUploadView(View):
    def get(self, request, id):
        photos_list = Photo.objects.filter(hotel=id)
        return render(self.request, 'staff/hotel/photo_upload.html', {'photos': photos_list, 'hotelid': id})

    def post(self, request, id):
        form = PhotoForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            photo = form.save(commit=False)
            hotel = Hotels.objects.get(id=id)
            photo.hotel = hotel
            photo.save()
            data = {'is_valid': True, 'name': photo.path.name,
                    'url': photo.path.url}
        else:
            data = {is_valid: False}
        return JsonResponse(data)


class ChartView(View):
    def get(self, request, id):
        return render(request, 'staff/hotel/PartnerCharts.html')


# Using django rest to send data to he views.
class ChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, id, format=None):
        # Loads the hotel countires with a list of countries within which the partner operates
        # Get the number of Hotels in each country.
        HotelCountries = Hotels.objects.filter(
            Partner_id=id).values_list('Country', flat=True).distinct()
        count = []
        for Country in HotelCountries:
            count.append(Hotels.objects.filter(Country=Country).count())

        # Get the number of bookings made by each hotel
        HotelNames = Hotels.objects.filter(
            Partner_id=id).values_list('Name', flat=True)
        PartnerHotel = Hotels.objects.filter(Partner_id=id)
        bookings = []
        # calculate the amount of money each hotel has made this year.
        for Hotel in PartnerHotel:
            bookings.append(Reservation.objects.filter(hotel=Hotel).count())

        today = datetime.datetime.now()
        money = []
        for Hotel in PartnerHotel:
            money.append(Reservation.objects.filter(
                hotel=Hotel, CheckOut__year=today.year).aggregate(sum=Sum('totalPrice'))['sum'])

        data = {"Countries": HotelCountries, "Count": count,
                "Hotels": HotelNames, "Bookings": bookings, "Money": money}
        print(PartnerHotel)
        return Response(data)


# Partners can cancel a booking made by a user.
def cancelbooking(request, pk):
    Booking = Reservation.objects.get(id=pk)
    hotelid = Booking.hotel.id
    Booking.delete()
    link = reverse('hotel_admin:hotelreservations', args=[hotelid])
    return HttpResponseRedirect(link)


# ================================ Hotel ================================
# Generate Create Update and delete views using django's Createview
class HotelCreateView(CreateView):
    model = Hotels
    template_name = 'staff/hotel/hotel_form.html'
    fields = ['th_name', 'en_name', 'address', 'city',
              'telephone_number', 'th_brief_description', 'en_brief_description', 'partner']

    def get_success_url(self):
        #hotelid = self.kwargs['id']
        #url = reverse('HotelApp:hoteldetails', args=[hotelid])
        url = reverse('hotel_admin:home')
        return url

    def form_valid(self, form):
        user = self.request.user
        thepartner = Partners.objects.get(userID=user)
        form.instance.Partner_id = thepartner.id
        return super(HotelCreateView, self).form_valid(form)


class HotelUpdateView(UpdateView):
    model = Hotels
    template_name = 'staff/hotel/hotel_form.html'
    fields = ['th_name', 'en_name', 'address', 'city',
              'telephone_number', 'th_brief_description', 'en_brief_description', 'partner']

    def get_success_url(self):
        hotelid = self.kwargs['pk']
        return reverse('hotel_admin:photodash', args=[hotelid])

    def form_valid(self, form):
        return super(UpdateView, self).form_valid(form)


class HotelDeleteView(DeleteView):
    model = Hotels

    def get_success_url(self):
        return reverse('hotel_admin:hotel_view')

    def form_valid(self, form):
        return super(HotelDeleteView, self).form_valid(form)

# ================================ End Hotel ================================


# ================================ Room ================================
class RoomCreateView(CreateView):
    model = Room
    template_name = 'staff/hotel/room_form.html'
    fields = ['room_type', 'capacity', 'bed_option',
              'price', 'overview', 'total_rooms', 'photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5']


    def get_success_url(self):
        hotelid = self.kwargs['pk']
        url = reverse('hotel_admin:room_view', args=[hotelid])
        return url

    def form_valid(self, form):
        form.instance.hotel_id = self.kwargs['pk']
        return super(RoomCreateView, self).form_valid(form)


class RoomUpdateView(UpdateView):
    model = Room
    template_name = 'staff/hotel/room_form.html'
    fields = ['room_type', 'capacity', 'bed_option',
              'price', 'overview', 'total_rooms', 'photo_1', 'photo_2', 'photo_3', 'photo_4', 'photo_5']

    def get_success_url(self):
        roomid = self.kwargs['pk']
        room = Room.objects.get(id=roomid)
        hotel = room.hotel
        hotelid = hotel.id
        return reverse('hotel_admin:managehotel', args=[hotelid])

    def form_valid(self, form):
        return super(RoomUpdateView, self).form_valid(form)


class RoomDeleteView(DeleteView):
    model = Room

    def get_success_url(self):
        roomid = self.kwargs['pk']
        room = Room.objects.get(id=roomid)
        hotel = room.hotel
        hotelid = hotel.id
        url = reverse('hotel_admin:room_view', args=[hotelid])
        return url

# ================================ End Room ================================
