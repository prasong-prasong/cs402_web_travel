from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from question.decorators import staff_required
# from trip.models import Landmark


# @method_decorator([login_required, staff_required], name='dispatch')
# class LandmarkCreateView(CreateView):
#     model = Landmark
#     context_object_name = 'landmark'
#     template_name = 'staff/landmark/landmark_create.html'
#     fields = ['name', ]
#     def get_success_url(self):
#         return reverse_lazy('other_staff')


# @method_decorator([login_required, staff_required], name='dispatch')
# class LandmarkUpdateView(UpdateView):
#     model = Landmark
#     context_object_name = 'landmark'
#     template_name = 'staff/landmark/landmark_update.html'
#     fields = ['name', ]
#     def get_success_url(self):
#         return reverse_lazy('other_staff')


# @method_decorator([login_required, staff_required], name='dispatch')
# class LandmarkDeleteView(DeleteView):
#     model = Landmark
#     context_object_name = 'landmark'
#     template_name = 'staff/landmark/landmark_delete.html'
#     def get_success_url(self):
#         return reverse_lazy('other_staff')


