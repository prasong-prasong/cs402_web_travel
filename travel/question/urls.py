from django.urls import include, path

from question.views import question, active

urlpatterns = [

    path('active/', include(([
        path('', active.QuizListView.as_view(), name='quiz_list'),
        path('interests/', active.ActiveInterestsView.as_view(), name='active_interests'),
        path('taken/', active.TakenQuizListView.as_view(), name='taken_quiz_list'),
        path('quiz/<int:pk>/', active.take_quiz, name='take_quiz'),
    ], 'question'), namespace='active')),

]
